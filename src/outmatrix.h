/*
 * outmatrix.h
 *
 *  Created on: Apr 25, 2013
 *      Author: kristin
 */

#ifndef OUTMATRIX_H_
#define OUTMATRIX_H_

#include <fstream>
#include <cstdlib>
#include "matrix.h"
#include "point.h"
#include "common.h"
using namespace std;

class OutMatrix: public Matrix {

private:
	PointVector* vectors;

	uint numFiles;
	char path[BUF_SIZE];

	void sortElements();

	void writeToFile();

	void mergeToFile(const char* filename);

public:
	void compute();

	OutMatrix() :
			Matrix() {
		numFiles = 0;
		vectors = NULL;
	}

	OutMatrix(char * dataPath, PointVector* v, float threshold);

	~OutMatrix() {
		fprintf(stderr, "Number of files: %u\n", numFiles);
	}
};

#endif /* OUTMATRIX_H_ */
