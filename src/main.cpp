/*
 * main.cpp
 *
 *  Created on: Apr 5, 2013
 *      Author: kristin
 */
#include "common.h"
#include "outmatrix.h"

////////////////////////////////////////////////////////////////////////////////
// Program main
////////////////////////////////////////////////////////////////////////////////
bool verbose = false;

int main(int argc, char **argv) {

	uint numThreads = 1;
	float threshold = 1.0f;
	uint numPoints = 10;
	uint dim = 3;
	char path[BUF_SIZE];

	Timer timer;
	timer.tic();

	printf("\n=========================== Simulate ===========================\n");
	fprintf(stderr, "\n==== Extract parameters ====\n");

	printUsage();
	if (!extractOptions("--path=%s", path, argc, argv))
		sprintf(path, "%s", ".");
	printf("Data path: %s\n", path);

	if (!extractOptions("--size=%u", &numPoints, argc, argv))
		numPoints = 10;
	printf("Data points: %u\n", numPoints);

	if (!extractOptions("--dim=%u", &dim, argc, argv))
		dim = 3;
	printf("Dimension: %u\n", dim);

	if (!extractOptions("--threshold=%f", &threshold, argc, argv))
		threshold = 1.0f;
	printf("Threshold: %f\n", threshold);

	if (!extractOptions("--num-threads=%u", &numThreads, argc, argv))
		numThreads = omp_get_num_procs();
	omp_set_num_threads(numThreads);
	fprintf(stderr, "Number of threads: %u\n", numThreads);

	initProfiler(2, "point", "compute");

	fprintf(stderr, "\n==== Generate data points ====\n");
	PointVector* points = NULL;
	srand(time(NULL));
	PROFILE("point", points = new PointVector(numPoints, dim));

	char buf[BUF_SIZE];
	sprintf(buf, "%s/points_%u.dat", path, numPoints);
	points->printToFile(buf);

	fprintf(stderr, "\n==== Compute distance matrix ====\n");
	OutMatrix *outMat;
	outMat = new OutMatrix(path, points, threshold);
	PROFILE("compute", outMat->compute());
	outMat->stats();

	delete points;
	delete outMat;

	printProfilerStats(); // print profile statistics before exit
	timer.toc();
	printf("Total time taken: %.4lf seconds\n", timer.getLapse());
	return EXIT_SUCCESS;
}

