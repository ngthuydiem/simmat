/*
 * distance.h
 *
 *  Created on: Apr 8, 2013
 *      Author: kristin
 */

#ifndef DISTANCE_H_
#define DISTANCE_H_

#include "point.h"

float computeEuclidDist(Point* p1, Point* p2);

#endif /* DISTANCE_H_ */
