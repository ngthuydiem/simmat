################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/common.cpp \
../src/distance.cpp \
../src/main.cpp \
../src/matrix.cpp \
../src/outmatrix.cpp \
../src/point.cpp 

OBJS += \
./src/common.o \
./src/distance.o \
./src/main.o \
./src/matrix.o \
./src/outmatrix.o \
./src/point.o 

CPP_DEPS += \
./src/common.d \
./src/distance.d \
./src/main.d \
./src/matrix.d \
./src/outmatrix.d \
./src/point.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O3 -Wall -c -fmessage-length=0 -std=c++11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


